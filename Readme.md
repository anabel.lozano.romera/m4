<!DOCTYPE html>
<html lang="ca">
<head>
	<title> "INDEX"</title>
	<meta charset="UTF-8"/>
</head>
	<header>
		<h1>PLA D'ESTUDIS CFGS</h1>
  	<h1>ASIX-Administració de sistemes informàtica en xarxes</h1>
	</header>

<article>
  <section id="INDEX">
	<h1>UF1</h1>
		<ul>
			<li>
			<h4><a
				href="https://gitlab.com/anabel.lozano.romera/m4/blob/master/UF1/UF1_NF3_Introducció_les_pàgines_web_Repositori_Gitlab.html">UF1_NF3_Introducció_les_pàgines_web_Repositori_Gitlab.html</a></h4>
			</li>
		</ul>
      <h1>UF2</h1>
		<ul>
			<li>
				<h4><a href="https://gitlab.com/anabel.lozano.romera/m4/blob/master/UF2/P1/UF2-1-P1_Llistes_i_imatges">UF2-1-P1_Llistes_i_imatges</a></h4>
			</li>
		</ul>
        <ul>
            <li>
              <h4><a href="https://gitlab.com/anabel.lozano.romera/m4/blob/master/UF2/Act-2-Formulari/Act-2-Formulari.html">Act-2-Formulari:</a></h4>
            </li>
      </ul>
      <ul>
        <li>
          <h4><a href="https://gitlab.com/anabel.lozano.romera/m4/blob/master/UF2/Proyecto/Principal.html">Proyecto:</a></h4>
        </li>
      </ul>
       <ul>
        <li>
          <h4><a href="https://gitlab.com/anabel.lozano.romera/m4/blob/master/UF2/Pseudoclasses-hover/css3_c2-css-advanced_e5-pseudoclasses-hover.html">Pseudoclasses-hover:</a></h4>
        </li>
      </ul>
  </section>
</article>

<footer>
  <p class="author">Anabel Lozano Romera</p>

</footer>
</body>
</html>